import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {
  constructor(public http: HttpClient) { }

  getHTML(url, callback) {
    // Create new request
    var xhr = new XMLHttpRequest();

    // Setup callback
    xhr.onload = function () {
      if (callback && typeof (callback) === 'function') {
        callback(this.responseXML);
      }
    }

    // Get the HTML
    xhr.open('GET', url);
    xhr.responseType = 'document';
    xhr.send();
  };

  ngOnInit() {
    this.getHTML('https://content.cdn.dbp-prod.dbp4.org/text/NHOTCT/NHOTCT/NHOTCT_70_MAT_1.html?x-amz-transaction=4302466&Expires=1553032486&Signature=b69hjNWCuCvOgygUlrQmxPpJ4L7CWCvrVWpUADBzsoP7v38CfCc7MoZ18remdF472A3zkZEUtqFU6CNA8wZMHnPxv-auCXu0xoPx-9pGQdu6RQ1Jnph5oA6g-qmEsW3TS9m6xSrwniCz9C7C5ChXZRp-SN2Wjm-EVeyWQAqk-gqeV7ymKJvcf9tAlKvL6aTQf-61U5v9seEZNr6InjYG~ZS69LQBjgj9YLJugPdX5uE9uhJ-Vh9geHiqt78PGNjZ9cxtF2u5dW00BPR6XbTdNJLwbkx6Grig5PlAlo8X6RODKzUQg9liAfEXYcibe4kjwFo7ECwgD0ISEK1UJCbLVQ__&Key-Pair-Id=APKAI4ULLVMANLYYPTLQ', function (response) {
      var bodyHTML = response.querySelector('.chapter');
      var footnotesHTML = response.querySelector('.footnotes');
      var c = response.querySelector('.c');
      bodyHTML.removeChild(c);
      var ionicContent = document.querySelector('#formatted');
      ionicContent.innerHTML = bodyHTML.innerHTML;
      ionicContent.appendChild(footnotesHTML);
    });
  }
}

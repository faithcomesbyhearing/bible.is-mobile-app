import { Component, OnInit } from '@angular/core';
import { ReaderService } from '../../reader.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-languages',
  templateUrl: './languages.page.html',
  styleUrls: ['./languages.page.scss'],
})
export class LanguagesPage implements OnInit {
  isFiltered: boolean = false;

  languages: any[];
  formattedLanguages: any[];

  selectedCountry: any;
  selectedLanguage: any;

  constructor(public api: ReaderService, public router: Router) { }

  ngOnInit() {}

  ionViewWillEnter() {
    // Check to see if a language has already been selected
    if (this.api.getCurrentLanguage()) {
      this.selectedLanguage = this.api.getCurrentLanguage();
    }

    // If a country has been selected, filter the languages
    if (this.api.getCurrentCountry()) {
      this.selectedCountry = this.api.getCurrentCountry();
      this.isFiltered = true;

      this.getLanguagesForCountry(this.selectedCountry);
    } else if (this.api.getAllLanguages()) {
      // If a complete list of languages exists in memory, use it
      this.languages = this.api.getAllLanguages();
      this.formattedLanguages = this.formatLanguages();
    } else {
      // If a country has not been selected, and a full list of languages
      // does not exist in memory, fetch a fresh list of languages from the api
      this.getLanguages();
    }
  }

  getLanguages() {
    this.api.getLanguages()
      .subscribe(res => {
        // Save the list here
        this.languages = res.data;
        // Save the list in memory
        this.api.setAllLanguages(res.data);
        // Format the languages
        this.formattedLanguages = this.formatLanguages();
      }, err => {
        console.log(err);
      });
  }

  getLanguagesForCountry(country: any) {
    this.api.getLanguagesForCountry(country.codes.iso_a2)
      .subscribe(res => {
        // Save the list here
        this.languages = res.data;
      }, err => {
        console.log(err);
      });
  }

  compareLanguageNames(a: any, b: any) {
    if (a.name < b.name) {
      return -1;
    }

    if (a.name > b.name) {
      return 1;
    }

    return 0;
  }

  formatLanguages() {
    const sections: any[] = [];
    let currentSection = null;
    let currentLetter = '';

    this.languages.sort(this.compareLanguageNames);

    // Cycle through each language to create sections (alphabetical)
    this.languages.forEach((language) => {
      // If this language starts with a different letter,
      // the current section is over and a new one needs to
      // be created
      if (language.name.charAt(0) !== currentLetter) {
        if (currentSection != null) {
          // Add the current section to the sections list
          sections.push(currentSection);
        }

        // Update the current letter for the section title
        currentLetter = language.name.charAt(0);
        // Create the skeleton for the new section
        currentSection = { title: currentLetter.toUpperCase(), data: [] };
      }

      // Add the language to the current section
      currentSection.data.push(language);
    });

    return sections;
  }

  selectLanguage(language: any) {
    this.selectedLanguage = language;
    this.api.setCurrentLanguage(this.selectedLanguage);
    this.router.navigateByUrl('reader/bibles/selector/versions');
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { BiblesPage } from './bibles.page';

const routes: Routes = [
  {
    path: 'selector',
    component: BiblesPage,
    children: [
      {
        path: 'countries',
        children: [
          {
            path: '',
            loadChildren: './countries/countries.module#CountriesPageModule'
          }
        ]
      },
      {
        path: 'languages',
        children: [
          {
            path: '',
            loadChildren: './languages/languages.module#LanguagesPageModule'
          }
        ]
      },
      {
        path: 'versions',
        children: [
          {
            path: '',
            loadChildren: './versions/versions.module#VersionsPageModule'
          }
        ]
      }
    ]
  },
  {
    path: '',
    redirectTo: '/reader/bibles/selector/countries',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule],
  declarations: [BiblesPage]
})
export class BiblesPageModule {}

import { Component, OnInit } from '@angular/core';
import { ReaderService } from '../../reader.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-countries',
  templateUrl: './countries.page.html',
  styleUrls: ['./countries.page.scss'],
})
export class CountriesPage implements OnInit {
  countries: any[];
  formattedCountries: any[];

  selectedCountry: any;

  flag: string = 'assets/flags.svg#AF';

  constructor(public api: ReaderService, public router: Router) { }

  ngOnInit() { }

  ionViewWillEnter() {
    // Check to see if a country has already been selected
    if (this.api.getCurrentCountry()) {
      this.selectedCountry = this.api.getCurrentCountry();
    }

    // If a complete list of countries exists in memory, use it
    if (this.api.getAllCountries()) {
      this.countries = this.api.getAllCountries();
      this.formattedCountries = this.formatCountries();
    } else {
      // If a full list of countries does not exist in memory, 
      // fetch a fresh list of countries from the api
      this.getCountries();
    }
  }

  getCountries() {
    this.api.getCountries()
      .subscribe(res => {
        // Save the list here
        this.countries = res.data;
        // Save the list in memory
        this.api.setAllCountries(res.data);
        // Format the list of countries
        this.formattedCountries = this.formatCountries();
      }, err => {
        console.log(err);
      });
  }

  compareCountryNames(a: any, b: any) {
    if (a.name < b.name) {
      return -1;
    }

    if (a.name > b.name) {
      return 1;
    }

    return 0;
  }

  formatCountries() {
    const sections: any[] = [];
    let currentSection = null;
    let currentLetter = '';

    this.countries.sort(this.compareCountryNames);

    // Cycle through each country to create sections (alphabetical)
    this.countries.forEach((country) => {
      // If this country starts with a different letter,
      // the current section is over and a new one needs to
      // be created
      if (country.name.charAt(0) !== currentLetter) {
        if (currentSection != null) {
          // Add the current section to the sections list
          sections.push(currentSection);
        }

        // Update the current letter for the section title
        currentLetter = country.name.charAt(0);
        // Create the skeleton for the new section
        currentSection = { title: currentLetter.toUpperCase(), data: [] };
      }

      // Create the reference for the flag
      const flag = 'assets/flags.svg#' + country.codes.iso_a2;

      // Add the country to the current section
      currentSection.data.push({ ...country, flag });
    });

    return sections;
  }

  selectCountry(country: any) {
    this.selectedCountry = country;
    this.api.setCurrentCountry(this.selectedCountry);
    this.router.navigateByUrl('reader/bibles/selector/languages');
  }
}

import { Component, OnInit } from '@angular/core';
import { ReaderService } from '../reader.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-chapters',
  templateUrl: './chapters.page.html',
  styleUrls: ['./chapters.page.scss'],
})
export class ChaptersPage implements OnInit {
  books: any[];
  selectedBookId: string;
  selectedChapter: number;

  constructor(public api: ReaderService, public router: Router) { }

  ngOnInit() {}

  ionViewWillEnter() {
    if (this.api.getCurrentFilesetId()) {
      this.getBooksAndChapters(this.api.getCurrentFilesetId());
    }
  }

  getBooksAndChapters(filesetId: string) {
    this.api.getBooksAndChapters(filesetId, 'text_plain')
      .subscribe(res => {
        this.books = res.data;
      }, err => {
        console.log(err);
      });
  }

  selectBook(index: number) {
    this.selectedBookId = this.books[index].book_id;
    this.api.setCurrentBook(this.books[index]);
  }

  selectChapter(chapterIndex: number, bookIndex: number) {
    this.selectedChapter = this.books[bookIndex].chapters[chapterIndex];
    this.api.setCurrentChapter(this.selectedChapter);
    this.router.navigateByUrl('reader');
  }
}

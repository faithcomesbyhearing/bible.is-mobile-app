import { Component, OnInit } from '@angular/core';
import { ReaderService } from './reader.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-reader',
  templateUrl: './reader.page.html',
  styleUrls: ['./reader.page.scss'],
})
export class ReaderPage implements OnInit {
  versionsForSelectedLanguage: any[];
  verses: any[];

  selectedBookName: string;
  selectedVersion: any;
  selectedBookId: string;
  selectedChapter: number;

  isSwiping: boolean = false;
  touchStart: any;
  touchEnd: any;

  constructor(public api: ReaderService, public router: Router) { }

  ngOnInit() { }

  ionViewWillEnter() {
    // If a version is saved in memory, use it.
    // Otherwise, default to ENGESV.
    if (this.api.getCurrentVersion()) {
      this.selectedVersion = this.api.getCurrentVersion();
    } else {
      this.getVersionsForLanguage('ENG');
      return;
    }

    // If the current book is saved in memory, use it
    if (this.api.getCurrentBook() != null) {
      this.selectedBookId = this.api.getCurrentBook().book_id;
      this.selectedBookName = this.api.getCurrentBook().name;
    }

    // If the current chapter is saved in memory, use it
    if (this.api.getCurrentChapter() > 0) {
      this.selectedChapter = this.api.getCurrentChapter();
    }

    // Get the Bible text for the current chapter
    this.checkForBibleText();
  }

  getVersionsForLanguage(languangeCode: string) {
    this.api.getVersionsForLanguage(languangeCode)
      .subscribe(res => {
        this.versionsForSelectedLanguage = res.data;
        this.selectedVersion = this.versionsForSelectedLanguage[0];
        this.api.setCurrentVersion(this.selectedVersion);

        // Get the Bible text for the current chapter
        this.checkForBibleText();
      }, err => {
        console.log(err);
      });
  }

  getChapterText(bibleVersionId: string, selectedBook: string, selectedChapter: number) {
    this.api.getChapterText(bibleVersionId, selectedBook, selectedChapter)
      .subscribe(res => {
        this.verses = res.data;

        if (this.verses.length <= 0 && this.api.getCurrentFilesetId()) {
          this.getBooksAndChapters(this.api.getCurrentFilesetId());
        }
      }, err => {
        console.log(err);
      });
  }

  getBooksAndChapters(filesetId: string) {
    this.api.getBooksAndChapters(filesetId, 'text_plain')
      .subscribe(res => {
        if (res.data.length > 0) {
          this.api.setCurrentBook(res.data[0]);
          this.api.setCurrentChapter(res.data[0].chapters[0]);

          this.selectedBookId = this.api.getCurrentBook().book_id;
          this.selectedBookName = this.api.getCurrentBook().name;
          this.selectedChapter = this.api.getCurrentChapter();

          this.getChapterText(filesetId, this.selectedBookId, this.selectedChapter);
        }
      }, err => {
        console.log(err);
      });
  }

  checkForBibleText() {
    let filesetId = null;

    // Get the selected version's fileset ids
    if (this.selectedVersion != null) {
      filesetId = this.getVersionFilesetId(this.selectedVersion, 'text_plain');
    }

    // If text is available, get it
    if (filesetId != null) {
      this.api.setCurrentFilesetId(filesetId);

      // If the available books for this fileset
      // have not been loaded, get them
      if (this.selectedBookId == null || this.selectedChapter == null) {
        this.getBooksAndChapters(filesetId);
      } else {
        // Get the chapter's text
        this.getChapterText(filesetId, this.selectedBookId, this.selectedChapter);
      }
    }
  }

  getVersionFilesetId(version: any, type: string) {
    // If there is a fileset available, return it
    const match = version.filesets['dbp-prod'].find((fileset) => {
      return fileset.type === type;
    });

    // If the match is undefined, there is no fileset available
    if (match !== undefined) {
      return match.id;
    }

    return null;
  }

  goToChaptersPage() {
    this.router.navigateByUrl('reader/chapters');
  }

  goToBiblesPage() {
    this.router.navigateByUrl('reader/bibles');
  }

  onTouchStart(event: any) {
    if (this.isSwiping) {
      return;
    }

    this.isSwiping = true;
    this.touchStart = event.touches[0];
  }

  onTouchEnd(event: any) {
    if (!this.isSwiping) {
      return;
    }

    this.isSwiping = false;
    this.touchEnd = event.changedTouches[0];
    this.swipeToChangeChapter();
  }

  swipeToChangeChapter() {
    if ((this.touchStart.clientX - this.touchEnd.clientX) >= 100) {
      // User swiped left
      this.api.setCurrentChapter(this.selectedChapter++);

      // Get the Bible text for the current chapter
      this.checkForBibleText();
    } else if ((this.touchStart.clientX - this.touchEnd.clientX) <= -100) {
      // User swiped right
      this.api.setCurrentChapter(this.selectedChapter--);

      // Get the Bible text for the current chapter
      this.checkForBibleText();
    }
  }
}

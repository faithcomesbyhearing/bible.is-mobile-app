import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ReaderService {
  // DBP
  DBP_API_URL: string = 'https://api.dbp4.org/';
  DBP_API_KEY: string = 'cd27a52e-4b33-4ebb-bba7-e1d0289ab45c';
  DBP_API_VERSION: string = '4';
  DBP_API_FORMAT: string = 'json';

  private _allCountries: any[];
  private _allLanguages: any[];

  private _currentCountry: any;
  private _currentLanguage: any;
  private _currentVersion: any;
  private _currentFilesetId: string;

  private _currentBook: any = null;
  private _currentChapter: number = 0;

  constructor(private http: HttpClient) { }

  // Returns a list of Bible versions for the designated language
  getVersionsForLanguage(languageCode: string): Observable<any> {
    return this.http.get(`${this.DBP_API_URL}bibles?language_code=${languageCode.toUpperCase()}&v=
      ${this.DBP_API_VERSION}&key=${this.DBP_API_KEY}&format=${this.DBP_API_FORMAT}`);
  }

  // Returns a list of verses for the designated Bible version, book, and chapter
  getChapterText(bibleVersionId: string, bookId: string, chapter: number): Observable<any> {
    return this.http.get(`${this.DBP_API_URL}bibles/filesets/${bibleVersionId}/${bookId}/
      ${chapter}?v=${this.DBP_API_VERSION}&key=${this.DBP_API_KEY}&format=
      ${this.DBP_API_FORMAT}`);
  }

  // Returns a list of books and chapters for the designated Bible version
  getBooksAndChapters(bibleVersionId: string, filesetType: string): Observable<any> {
    return this.http.get(`${this.DBP_API_URL}bibles/filesets/${bibleVersionId}/books/?v=${this.DBP_API_VERSION}&key=
      ${this.DBP_API_KEY}&pretty=true&format=${this.DBP_API_FORMAT}&fileset_type=${filesetType}`);
  }

  // Returns a signed URL for formatted text
  getFormattedText(bibleVersionId: string, bookId: string, chapter: number): Observable<any> {
    return this.http.get(`${this.DBP_API_URL}bibles/filesets/${bibleVersionId}?v=${this.DBP_API_VERSION}&key=
      ${this.DBP_API_KEY}&format=${this.DBP_API_FORMAT}&book_id=${bookId}&chapter_id=${chapter}&type=text_format`);
  }

  // Returns a list of all countries with filesets
  getCountries(): Observable<any> {
    return this.http.get(`${this.DBP_API_URL}countries/?v=${this.DBP_API_VERSION}&key=
      ${this.DBP_API_KEY}&has_filesets=true`);
  }

  // Returns a list of all languages with filesets
  getLanguages(): Observable<any> {
    return this.http.get(`${this.DBP_API_URL}languages/?has_filesets=true&has_bibles=true&v=
      ${this.DBP_API_VERSION}&key=${this.DBP_API_KEY}&format=${this.DBP_API_FORMAT}`);
  }

  // Returns a list of languages for the designated country
  getLanguagesForCountry(country: string): Observable<any> {
    return this.http.get(`${this.DBP_API_URL}languages/?country=${country}&has_filesets=true&has_bibles=true&v=
      ${this.DBP_API_VERSION}&key=${this.DBP_API_KEY}&format=${this.DBP_API_FORMAT}`);
  }

  setAllCountries(countries: any[]) {
    this._allCountries = countries;
  }

  getAllCountries() {
    return this._allCountries;
  }

  setAllLanguages(languages: any[]) {
    this._allLanguages = languages;
  }

  getAllLanguages() {
    return this._allLanguages;
  }

  setCurrentCountry(country: any) {
    this._currentCountry = country;
  }

  getCurrentCountry() {
    return this._currentCountry;
  }

  setCurrentLanguage(language: any) {
    this._currentLanguage = language;
  }

  getCurrentLanguage() {
    return this._currentLanguage;
  }

  setCurrentVersion(version: any) {
    this._currentVersion = version;
  }

  getCurrentVersion() {
    return this._currentVersion;
  }

  setCurrentFilesetId(filesetId: string) {
    this._currentFilesetId = filesetId;
  }

  getCurrentFilesetId() {
    return this._currentFilesetId;
  }

  setCurrentChapter(chapter: number) {
    this._currentChapter = chapter;
  }

  getCurrentChapter() {
    return this._currentChapter;
  }

  setCurrentBook(book: any) {
    this._currentBook = book;
  }

  getCurrentBook() {
    return this._currentBook;
  }
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { ReaderPage } from './reader.page';

const routes: Routes = [
  {
    path: '',
    component: ReaderPage
  },
  {
    path: 'chapters',
    loadChildren: './chapters/chapters.module#ChaptersPageModule'
  },
  { path: 'bibles', loadChildren: './bibles/bibles.module#BiblesPageModule' }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    RouterModule.forChild(routes)
  ],
  declarations: [ReaderPage]
})
export class ReaderPageModule {}

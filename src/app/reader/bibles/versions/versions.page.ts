import { Component, OnInit } from '@angular/core';
import { ReaderService } from '../../reader.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-versions',
  templateUrl: './versions.page.html',
  styleUrls: ['./versions.page.scss'],
})
export class VersionsPage implements OnInit {
  versions: any[];
  formattedVersions: any[];

  selectedLanguage: any;
  selectedVersion: any;

  constructor(public api: ReaderService, public router: Router) { }

  ngOnInit() { }

  ionViewWillEnter() {
    // Check to see if a version has already been selected
    if (this.api.getCurrentVersion()) {
      this.selectedVersion = this.api.getCurrentVersion();
    }

    // If a language has been selected, get a list of available versions
    if (this.api.getCurrentLanguage()) {
      this.selectedLanguage = this.api.getCurrentLanguage();

      this.getVersionsForLanguage(this.selectedLanguage);
    }
  }

  getVersionsForLanguage(language: any) {
    this.api.getVersionsForLanguage(language.iso)
      .subscribe(res => {
        // Save the list here
        this.versions = res.data;
        // Format the versions
        this.formattedVersions = this.formatVersions();
      }, err => {
        console.log(err);
      });
  }

  compareVersionNames(a: any, b: any) {
    if (a.name < b.name) {
      return -1;
    }

    if (a.name > b.name) {
      return 1;
    }

    return 0;
  }

  formatVersions() {
    const sections = [];
    const audioAndText = {
      title: 'Audio & Text',
      data: []
    };
    const audioOnly = {
      title: 'Audio Only',
      data: []
    };
    const textOnly = {
      title: 'Text Only',
      data: []
    };

    // Cycle through each version to create sections
    this.versions.forEach((version) => {
      // Get a list of unique file types
      const types = version.filesets['dbp-prod'].reduce((accumulator, fileset) => {
        // Add the file type to the list
        return { ...accumulator, [fileset.type]: true };
      }, {});

      if ((types.audio_drama || types.audio)
        && (types.text_format || types.text_plain)) {
        // This version has audio and text available
        audioAndText.data.push(version);
      } else if (types.audio_drama || types.audio) {
        // This version has only audio available
        audioOnly.data.push(version);
      } else if (types.text_format || types.text_plain) {
        // This version has only text available
        textOnly.data.push(version);
      }
    });

    if (audioAndText.data.length > 0) {
      sections.push(audioAndText);
    }

    if (audioOnly.data.length > 0) {
      sections.push(audioOnly);
    }

    if (textOnly.data.length > 0) {
      sections.push(textOnly);
    }

    return sections;
  }

  selectVersion(version: any) {
    this.selectedVersion = version;
    this.api.setCurrentVersion(this.selectedVersion);
    this.router.navigateByUrl('reader');
  }
}

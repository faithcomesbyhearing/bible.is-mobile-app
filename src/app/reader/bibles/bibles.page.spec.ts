import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BiblesPage } from './bibles.page';

describe('BiblesPage', () => {
  let component: BiblesPage;
  let fixture: ComponentFixture<BiblesPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BiblesPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BiblesPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
